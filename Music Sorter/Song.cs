﻿using System.IO;

namespace MusicSorter {
	public struct Song {
		public string FilePath {
			get;
		}

		public string Artist {
			get;
		}

		public string Album {
			get;
		}

		public string Title {
			get;
		}

		public string FileName {
			get;
		}

		public uint? TrackNum {
			get;
		}

		public Song( string path, string artist, string album, string title, string fileName, uint? trackNum ) {
			FileName = fileName;
			FilePath = path;
			Artist = artist;
			Album = album;
			Title = title;
			TrackNum = trackNum;
		}

		public Song( string path, string artist, string album, string title, string fileName ) : this( path, artist, album, title, fileName, null ) {}

		public Song( string path, string artist, string album, string title ) : this( path.Substring( 0, path.LastIndexOf( Path.DirectorySeparatorChar ) ), artist, album, title, path.Substring( path.LastIndexOf( Path.DirectorySeparatorChar ) + 1 ) ) { }

		public Song( FileInfo file, string artist, string album, string title ) : this( file.FullName, artist, album, title ) { }

		public override string ToString() {
			return $"Artist: {Artist}\tAlbum: {Album}\tTitle: {Title}\tPath: {FilePath}\tFileName: {FileName}";
		}
	}
}
