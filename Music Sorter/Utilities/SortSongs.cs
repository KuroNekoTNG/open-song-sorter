﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Logging;

namespace MusicSorter.Utilities {
	internal static class SortSongs {

		private readonly static char[] AllInvalidElements;

		private static LogTalker log;

		static SortSongs() {
			List<char> invalids = new List<char>( Path.GetInvalidFileNameChars() );
			log = new LogTalker( typeof( SortSongs ) );

			invalids.AddRange( Path.GetInvalidPathChars() );

			for ( int i = 0; i < invalids.Count - 1; ++i ) {
				for ( int j = i + 1; j < invalids.Count; ++j ) {
					if ( invalids[i] == invalids[j] ) {
						invalids.RemoveAt( j-- );
					}
				}
			}

			AllInvalidElements = invalids.ToArray();
		}

		public static void StartSongSorting( in IEnumerable<Song> songs, in string sortPattern, in string outDir ) {
			string tempFolder = MoveFiles( songs );
			string newPath;

			foreach ( Song song in songs ) {
				newPath = BuildNewOutputDirPath( song, sortPattern, outDir );

				if ( !Directory.Exists( newPath ) ) {
					Directory.CreateDirectory( newPath );
				}

				newPath = Path.Combine( newPath, BuildFileName( song, sortPattern ) );

				try {
					File.Move( Path.Combine( tempFolder, song.FileName ), newPath );
				} catch ( Exception e ) {
					log.WriteException( $"An exception occured while doing job. Skipping file.", e, false );
				}
			}
		}

		private static string BuildFileName( in Song song, string sortPattern ) {
			StringBuilder builder = new StringBuilder();

			if ( sortPattern.LastIndexOf( Path.DirectorySeparatorChar ) != -1 ) {
				sortPattern = sortPattern.Substring( sortPattern.LastIndexOf( Path.DirectorySeparatorChar ) + 1 );
			} else {
				sortPattern = sortPattern.Substring( sortPattern.LastIndexOf( Path.AltDirectorySeparatorChar ) + 1 );
			}

			foreach ( string segment in sortPattern.Split( ' ' ) ) {
				string data = GrabCorrospondingSegment( song, segment ).Trim();

				if ( !string.IsNullOrWhiteSpace( data ) ) {
					builder.Append( data ).Append( ' ' );
				}
			}

			builder.Remove( builder.Length - 1, 1 ).Append( song.FileName.Substring( song.FileName.LastIndexOf( '.' ) ) );

			return NormalizeFileName( builder.ToString().Trim() );
		}

		private static string BuildNewOutputDirPath( in Song song, string sortPattern, in string dir ) {
			StringBuilder builder = new StringBuilder();
			string result;

			try {
				sortPattern = sortPattern.Substring( 0, sortPattern.LastIndexOf( Path.DirectorySeparatorChar ) );
			} catch {
				sortPattern = sortPattern.Substring( 0, sortPattern.LastIndexOf( Path.AltDirectorySeparatorChar ) );
			}

			foreach ( string segment in sortPattern.Split( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar, ' ' ) ) {
				string data = GrabCorrospondingSegment( song, segment );

				if ( data != null ) {
					if( 0 < builder.Length ) {
						builder.Append( Path.DirectorySeparatorChar );
					}

					builder.Append( data );
				}
			}

			result = builder.ToString();

			NormalizePathAndFileName( ref result );

			return Path.Combine( dir, result );
		}

		private static string GrabCorrospondingSegment( in Song song, in string segment ) {
			switch ( segment ) {
				case "${Title}":
					return song.Title;
				case "${Album}":
					return song.Album;
				case "${Artist}":
					return song.Artist;
				case "${TrackNum}":
					return song.TrackNum is null ? null : ( ( uint )song.TrackNum ).ToString();
				default:
					return $" {segment}";
			}
		}

		private static string MoveFiles( in IEnumerable<Song> songs ) {
			string tempFolder = Path.Combine( Path.GetTempPath(), "MusicSort.tmp" );

			if ( Directory.Exists( tempFolder ) ) {
				Directory.Delete( tempFolder, true );
			}

			Directory.CreateDirectory( tempFolder );

			foreach ( Song song in songs ) {
				File.Move( Path.Combine( song.FilePath, song.FileName ), Path.Combine( tempFolder, song.FileName ) );
			}

			return tempFolder;
		}

		private static string NormalizeFileName( string newFileName ) {
			NormalizeWithCharArray( ref newFileName, Path.GetInvalidFileNameChars() );

			return newFileName;
		}

		private static void NormalizePathAndFileName( ref string newPath ) {
			List<char> illegal = new List<char>( Path.GetInvalidPathChars() ) {
				':'
			};

			NormalizeWithCharArray( ref newPath, illegal );
		}

		private static void NormalizeWithCharArray( ref string str, IEnumerable<char> chars ) {
			foreach ( char badElement in chars ) {
				if ( str.Contains( badElement.ToString() ) ) {
					while ( str.IndexOf( badElement ) != -1 ) {
						str = str.Remove( str.IndexOf( badElement ), 1 );
					}
				}
			}
		}
	}
}