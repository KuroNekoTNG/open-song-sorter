﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Logging;

using File = TagLib.File;

namespace MusicSorter.Utilities {
	internal static class SongGather {

		// Huge search string for song files.
		public const string SONG_FILES = "*.mp3|*.flac|*.alac|*.wav|*.ogg|*.opus|*.aa|*.aax|*.aac|*.aiff|*.ape|*.dsf|*.m4a|*.m4b|*.m4p|*.mpc|*.mpp|*.oga|*.wma|*.wv|*.webm";

		private static LogTalker log;

		static SongGather() {
			log = new LogTalker( typeof( SongGather ) );
		}

		public static List<Song> GatherSongs( DirectoryInfo dir, bool topLevelOnly = false ) {
			List<Song> songs = new List<Song>();
			SearchOption searchOption;

			log.WriteVerbose( "Checking which search option to use." );

			searchOption = topLevelOnly ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories;

			log.WriteVerbose( "Looping through types of songs." );

			foreach ( string songFileSearch in SONG_FILES.Split( '|' ) ) {

				Debug.WriteLine( $"Searching '{dir.FullName}' with search pattern '{songFileSearch}'." );
				log.WriteVerbose( $"Searching '{dir.FullName}' with search pattern '{songFileSearch}'." );

				foreach ( FileInfo songFile in dir.EnumerateFiles( songFileSearch, searchOption ) ) {
					File file = File.Create( songFile.FullName );

					log.WriteVerbose( $"Created a '{file.GetType().FullName}'. Adding it to a list." );

					songs.Add( new Song( songFile.Directory.FullName, file.Tag.FirstPerformer, file.Tag.Album, file.Tag.Title, songFile.Name, file.Tag.Track ) );

					log.WriteVerbose( $"Finished creating song file and data." );
				}
			}

			log.WriteVerbose( "Finished, returning list." );

			return songs;
		}

		public static List<Song> GatherSongs( string path, bool topLevelOnly = false ) {
			log.WriteVerbose( "Forwarding request to other method." );

			return GatherSongs( new DirectoryInfo( path ), topLevelOnly );
		}
	}
}