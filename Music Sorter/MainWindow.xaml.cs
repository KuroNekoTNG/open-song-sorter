﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

using Logging;

using MusicSorter.Utilities;

using Ookii.Dialogs.Wpf;

namespace MusicSorter {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		private LogTalker log;
		private VistaFolderBrowserDialog openMusicDir;
		private ObservableCollection<Song> songs;

		public MainWindow() {
			log = new LogTalker( this );

			songs = new ObservableCollection<Song>();

			openMusicDir = new VistaFolderBrowserDialog() {
				RootFolder = Environment.SpecialFolder.UserProfile,
				ShowNewFolderButton = false
			};

			InitializeComponent();

			dataGrid.ItemsSource = songs;

			Closed += MainWindowClosed;

			log.WriteVerbose( "This is a test." );
			log.WriteException( "This is a test exception.", false );
			log.WriteError( "This is a test error." );
		}

		private void MainWindowClosed( object sender, EventArgs e ) {
#pragma warning disable CS0618 // Type or member is obsolete
			LogTalker.FinishWriting();
#pragma warning restore CS0618 // Type or member is obsolete
		}

		private void HelpButtonClicked( object sender, RoutedEventArgs e ) {
			AboutWindow about;

			if ( sender == helpButton ) {
				about = new AboutWindow();
				about.ShowDialog();
			}
		}

		private void OpenMusicDirectory( object sender, RoutedEventArgs e ) {
			bool successful;

			log.WriteVerbose( "Checking to verify sender." );

			if ( sender == openDirectoryWithMusic ) {
				successful = openMusicDir.ShowDialog( this ) ?? false;

				log.WriteVerbose( "Checking to see if dialog was successful." );

				if ( successful ) {
					foreach ( Song song in SongGather.GatherSongs( openMusicDir.SelectedPath ) ) {
						log.WriteVerbose( $"Adding song: {song.ToString()}." );

						songs.Add( song );
					}
				}
			}
		}

		private void StartSorting( object sender, RoutedEventArgs e ) {
			SortSongs.StartSongSorting( songs, fileFolderStructure.Text, outputLocation.Text );
			songs.Clear();
		}
	}
}
